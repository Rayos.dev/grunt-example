// Create a Gulp or Grunt file. 
// It should minify images 
// It should reload the browser when a file changes. 
// It should compile Less stylesheets into a .css file.

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        
        less: {
            development: {
                options: {
                    paths: ['css']
                },
                files: {
                'css/style.css': 'less/style.less'
                }
            }
        },

        imagemin: {
            static: {
                files: {
                    './img/img.jpg': './img/src/img.jpg',
                }
            }
        },


        watch: {
            css: {
                files: './less/style.less',
                tasks: ['less']
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './css/style.css',
                        '*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: './'
                }
            }
        }
    })
    grunt.loadNpmTasks('grunt-contrib-imagemin')
    grunt.loadNpmTasks('grunt-contrib-less')
    grunt.loadNpmTasks('grunt-contrib-watch')
    grunt.loadNpmTasks('grunt-browser-sync')
    grunt.registerTask('default', ['imagemin','browserSync','watch'])
}